# -*- coding: utf-8 -*-
from odoo import http
import base64
import urllib2
import marvelous


class Marvel(http.Controller):

    @http.route('/marvel/api/', auth='public', website=True)
    def index(self, **kw):
        return http.request.render('marvel.search', {})

    @http.route('/search/<qwery>', auth='public', website=True)
    def list_result(self, qwery, **kw):
        config_parameters = http.request.env['ir.config_parameter']
        m = marvelous.api(
            config_parameters.get_param('marvel.public_key'),
            config_parameters.get_param('marvel.private_key')
        )
        request = m.comics({
            'format': "comic",
            'formatType': "comic",
            'titleStartsWith': qwery,
            'orderBy': "-onsaleDate",
            'limit': 5})
        comics_list = request.response.get('data').get('results')
        for comic in comics_list:
            dates = comic.get('dates')
            if len(dates) > 0:
                for date in dates:
                    if date.get('type') == 'onsaleDate':
                        comic['onsaleDate'] = date.get('date').encode('utf-8').split('T')[0]
            images = comic.get('images')
            if len(images) > 0:
                path = images[0].get('path')
                image_variant = 'portrait_incredible'
                extension = images[0].get('extension')
                image_url = '/'.join([path, '.'.join([image_variant, extension])])
                comic['image_url'] = image_url

        return http.request.render('marvel.result', {
            'qwery': qwery,
            'comics_list': comics_list,
        })

    @http.route('/comic/detail/<int:id>/', auth='public', website=True)
    def detail(self, id, **kw):
        config_parameters = http.request.env['ir.config_parameter']
        m = marvelous.api(
            config_parameters.get_param('marvel.public_key'),
            config_parameters.get_param('marvel.private_key')
        )
        request = m.call(
            ['comics', id],
            {'limit': 1})
        comics_list = request.get('data').get('results')
        for comic in comics_list:
            dates = comic.get('dates')
            if len(dates) > 0:
                for date in dates:
                    if date.get('type') == 'onsaleDate':
                        comic['onsaleDate'] = date.get('date').encode('utf-8').split('T')[0]
            images = comic.get('images')
            if len(images) > 0:
                images_list = []
                for image in images:
                    path = image.get('path')
                    image_variant = 'portrait_incredible'
                    extension = image.get('extension')
                    image_url = '/'.join([path, '.'.join([image_variant, extension])])
                    images_list.append(image_url)
                comic['images_list'] = images_list
            characters = comic.get('characters').get('items')
            if len(characters) > 0:
                characters_list = []
                for character in characters:
                    characters_list.append(character.get('name'))
                comic['characters_list'] = characters_list
            stories = comic.get('stories').get('items')
            if len(stories) > 0:
                stories_list = []
                for story in stories:
                    stories_list.append(story.get('name'))
                comic['stories_list'] = stories_list

        return http.request.render('marvel.detail', {
            'comics_list': comics_list,
        })

    @http.route('/comic/save/<int:id>/', auth='public', type='http', website=True)
    def save(self, id, **kw):
        marvel_model = http.request.env['marvel.comics']
        config_parameters = http.request.env['ir.config_parameter']
        m = marvelous.api(
            config_parameters.get_param('marvel.public_key'),
            config_parameters.get_param('marvel.private_key')
        )
        request = m.call(
            ['comics', id],
            {'limit': 1})
        comics_list = request.get('data').get('results')
        for comic in comics_list:
            dates = comic.get('dates')
            if len(dates) > 0:
                for date in dates:
                    if date.get('type') == 'onsaleDate':
                        comic['onsaleDate'] = date.get('date').encode('utf-8').split('T')[0]
            images = comic.get('images')
            if len(images) > 0:
                images_list = []
                for image in images:
                    path = image.get('path')
                    image_variant = 'portrait_incredible'
                    extension = image.get('extension')
                    image_url = '/'.join([path, '.'.join([image_variant, extension])])
                    images_list.append(image_url)
                comic['images_list'] = images_list
            characters = comic.get('characters').get('items')
            if len(characters) > 0:
                characters_list = []
                for character in characters:
                    characters_list.append(character.get('name'))
                comic['characters_list'] = characters_list
            stories = comic.get('stories').get('items')
            if len(stories) > 0:
                stories_list = []
                for story in stories:
                    stories_list.append(story.get('name'))
                comic['stories_list'] = stories_list
        comic = comics_list[0]
        comic_db = marvel_model.search([('ean', '=', id)])
        if len(comic_db) > 0:
            comic_db.write({
                'ean': id,
                'title': comic.get('title'),
                'onsale_date': comic.get('onsaleDate'),
                'series': comic.get('series').get('name'),
                'description': comic.get('description'),
                'cover': base64.encodestring(urllib2.urlopen(comic.get('images_list')[0]).read()) if
                comic.get('images_list')[0] else False,
                'character_ids': [(1, 0, {'name': character}) for character in
                                  comic.get('characters_list')] if comic.get('characters_list') else False,
                'stories_ids': [(1, 0, {'name': story}) for story in comic.get('stories_list')] if comic.get(
                    'stories_list') else False,
                'images_ids': [(1, 0, {'url': image}) for image in comic.get('images_list')] if comic.get(
                    'images_list') else False
            })
        else:
            marvel_model.create({
                'ean': id,
                'title': comic.get('title'),
                'onsale_date': comic.get('onsaleDate'),
                'series': comic.get('series').get('name'),
                'description': comic.get('description'),
                'cover': base64.encodestring(urllib2.urlopen(comic.get('images_list')[0]).read()) if comic.get(
                    'images_list')[0] else False,
                'character_ids': [(0, 0, {'name': character}) for character in
                                  comic.get('characters_list')] if comic.get('characters_list') else False,
                'stories_ids': [(0, 0, {'name': story}) for story in comic.get('stories_list')] if comic.get(
                    'stories_list') else False,
                'images_ids': [(0, 0, {'url': image,
                                       'image': base64.encodestring(urllib2.urlopen(image).read())}) for image in
                               comic.get('images_list')] if comic.get('images_list') else False
            })
        return http.request.render('marvel.search', {})
