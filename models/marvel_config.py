# -*- coding: utf-8 -*-

from openerp import models, fields
from settings import ConfigSetterGetterMixin


class MarvelApiConfigSettings(models.TransientModel, ConfigSetterGetterMixin):
    _name = 'marvel_api.config.settings'
    _inherit = 'res.config.settings'

    public_key = fields.Char('PUBLIC_KEY')
    private_key = fields.Char('PRIVATE_KEY')

    # When you press Apply
    def set_public_key(self):
        return self.settings_setter('marvel',
                                    'public_key')

    # When page loads
    def get_default_public_key(self, fields):
        return self.settings_getter('marvel',
                                    'public_key')

    # When you press Apply
    def set_private_key(self):
        return self.settings_setter('marvel',
                                    'private_key')

    # When page loads
    def get_default_private_key(self, fields):
        return self.settings_getter('marvel',
                                    'private_key')
