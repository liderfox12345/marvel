# -*- coding: utf-8 -*-

from odoo import models, fields


class MarvelComics(models.Model):
    _name = 'marvel.comics'

    ean = fields.Integer(string='EAN')
    cover = fields.Binary(string='Cover')
    title = fields.Char(string='Title')
    onsale_date = fields.Date(string='On sale Date')
    series = fields.Char(string='Series')
    description = fields.Text(string='Description')
    character_ids = fields.One2many(string='Characters', comodel_name='comic.characters', inverse_name='comic_id')
    stories_ids = fields.One2many(stirng='Stories', comodel_name='comic.stories', inverse_name='comic_id')
    images_ids = fields.One2many(stirng='Images', comodel_name='comic.images', inverse_name='comic_id')


class ComicCharacters(models.Model):
    _name = 'comic.characters'

    comic_id = fields.Many2one(comodel_name='marvel.comics')
    name = fields.Char()


class ComicStories(models.Model):
    _name = 'comic.stories'

    comic_id = fields.Many2one(comodel_name='marvel.comics')
    name = fields.Char()


class ComicImages(models.Model):
    _name = 'comic.images'
    _rec_name = 'url'
    comic_id = fields.Many2one(comodel_name='marvel.comics')
    url = fields.Char(stirng='URL')
    image = fields.Binary(string='Image')
