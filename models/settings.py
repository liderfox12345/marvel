# -*- coding: utf-8 -*-


class ConfigSetterGetterMixin(object):
    # When you press Apply
    def settings_setter(self, module, field):
        """
        Getter for config settings getter interface

        :param module:
        :param field:
        :param cr:
        :param uid:
        :param ids:
        :param context:
        :return:
        """
        config_parameters = self.env['ir.config_parameter']
        config_param_name = '.'.join([module, field]);
        for record in self:
            config_parameters.set_param(config_param_name,
                                        getattr(record, field) or '')
        pass

    # When page loads
    def settings_getter(self, module, field):
        """
        Setter for settings interface

        :param str module:
        :param str field:
        :param cr:
        :param uid:
        :param fields:
        :param context:
        :return:
        """
        config_param_name = '.'.join([module, field]);
        value = self.env['ir.config_parameter'].get_param(config_param_name, '')
        return {field: value}